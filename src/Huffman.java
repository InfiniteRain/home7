
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 *
 * USED SOURCES:
 * https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/
 * http://enos.itcollege.ee/~ylari/I231/Huffman.java
 */
public class Huffman {
   public class HuffmanTreeNode {
      public Byte value;
      public int frequency;
      public HuffmanTreeNode left;
      public HuffmanTreeNode right;
   }

   public class HuffmanTreeNodeComparator implements Comparator<HuffmanTreeNode> {
      @Override
      public int compare(HuffmanTreeNode o1, HuffmanTreeNode o2) {
         return o1.frequency - o2.frequency;
      }
   }

   public HuffmanTreeNode root;
   public int length;

   /** Constructor to build the Huffman code for a given byte array.
    * @param original source data
    */
   Huffman (byte[] original) {
      if (original.length == 0) {
         throw new RuntimeException("Zero-length byte array was provided.");
      }

      HashMap<Byte, Integer> frequencyList = new HashMap<>();

      for (byte part : original) {
         if (frequencyList.containsKey(part)) {
            int newFrequency = frequencyList.get(part) + 1;
            frequencyList.put(part, newFrequency);
         } else {
            frequencyList.put(part, 1);
         }
      }

      PriorityQueue<HuffmanTreeNode> queue = new PriorityQueue<>(frequencyList.size(), new HuffmanTreeNodeComparator());

      for (Map.Entry<Byte, Integer> entry : frequencyList.entrySet()) {
         HuffmanTreeNode node = new HuffmanTreeNode();

         node.value = entry.getKey();
         node.frequency = entry.getValue();

         queue.add(node);
      }

      if (queue.size() == 1) {
         root = new HuffmanTreeNode();
         root.frequency = 1;

         root.left = new HuffmanTreeNode();
         root.left.value = queue.peek().value;

         return;
      }

      HuffmanTreeNode root = null;

      while (queue.size() > 1) {
         HuffmanTreeNode x = queue.poll();
         HuffmanTreeNode y = queue.poll();

         HuffmanTreeNode localRoot = new HuffmanTreeNode();
         localRoot.frequency = x.frequency + y.frequency;
         localRoot.left = x;
         localRoot.right = y;

         root = localRoot;
         queue.add(localRoot);
      }

      this.root = root;
   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      return length;
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      HashMap<Byte, String> bitMap = getBitMap(this.root);
      StringBuffer sb = new StringBuffer();

      for (byte b : origData) {
         sb.append(bitMap.get(b));
      }

      length = sb.length();
      ArrayList<Byte> bytes = new ArrayList<>();

      while (sb.length() > 0) {
         while (sb.length() < 8) {
            sb.append('0');
         }

         String byteString = sb.substring(0, 8);
         bytes.add((byte) Integer.parseInt(byteString, 2));
         sb.delete(0, 8);
      }

      byte[] encoded = new byte[bytes.size()];
      for (int i = 0; i < bytes.size(); i++) {
         encoded[i] = bytes.get(i);
      }

      return encoded;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
      StringBuilder sb = new StringBuilder();

      for (byte encodedDatum : encodedData) {
         sb.append(String.format("%8s", Integer.toBinaryString(encodedDatum & 0xFF)).replace(' ', '0'));
      }

      String encodedString = sb.substring(0, length);
      List<Byte> bytes = new ArrayList<>();
      String sequence = "";

      while (encodedString.length() > 0) {
         sequence += encodedString.substring(0, 1);
         encodedString = encodedString.substring(1);

         for (Map.Entry<Byte, String> entry : getBitMap(this.root).entrySet()) {
            if (entry.getValue().equals(sequence)) {
               bytes.add(entry.getKey());
               sequence = "";
               break;
            }
         }
      }

      byte[] decoded = new byte[bytes.size()];
      for (int i = 0; i < bytes.size(); i++) {
         decoded[i] = bytes.get(i);
      }

      return decoded;
   }

   public static HashMap<Byte, String> getBitMap(HuffmanTreeNode root) {
      HashMap<Byte, String> map = new HashMap<>();
      return getBitMap(root, "", map);
   }

   public static HashMap<Byte, String> getBitMap(
           HuffmanTreeNode root,
           String sequence,
           HashMap<Byte, String> map
   ) {
      if (root.left == null && root.right == null && root.value != null) {
         map.put(root.value, sequence);

         return map;
      }

      if (root.left != null) {
         getBitMap(root.left, sequence + "0", map);
      }

      if (root.right != null) {
         getBitMap(root.right, sequence + "1", map);
      }

      return map;
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "ABCDEFAAABBC";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);

      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      System.out.println (Arrays.equals (orig, orig2));

      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }
}

